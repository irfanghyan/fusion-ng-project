import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const accountModule = () => import('./account/account.module').then(x => x.AccountModule);
const dashboardModule = () => import('./dashboard/dashboard.module').then(x => x.DashboardModule);

const routes: Routes = [

    {
      path: 'account',
      loadChildren: accountModule },
    {
      path: 'dashboard',
      loadChildren: dashboardModule },

    { path: '**', redirectTo: 'account/signin' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
