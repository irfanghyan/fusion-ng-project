import { Component, Input, OnInit, } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-project-card',
  templateUrl: './project-card.component.html',
  styleUrls: ['./project-card.component.scss']
})
export class ProjectCardComponent implements OnInit {

  @Input('data') data: any;

  constructor(
    private _router: Router,

  ) { }

  ngOnInit(): void {
  }

  moveToNextPage(project: any) {
    this._router.navigateByUrl('dashboard/application')
  }
}
