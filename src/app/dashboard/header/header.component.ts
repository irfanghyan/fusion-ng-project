import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  [x: string]: any;
  dropdownBtn: any;
  projectMenuFlag: boolean = false;
  settingsMenuFlag: boolean = false;
  notificationMenuFlag: boolean = false;
  notificationSettingMenuFlag: boolean = false;
  integrationMenuFlag: boolean = false;
  adminFlag: boolean = false;

  selectedProject: any;
  showToggleMenu = false;
  brandIcons = [
    '../../../assets/images/Icon_Android_24.svg#Icon_Android_24',
    '../../../assets/images/Icon_Apple_24.svg#Icon_Apple_24'

  ];
  cd: any;

  constructor(
      public __router: Router,

  ) { }

  ngOnInit(): void {


  }

  signOut() {
    localStorage.clear();
    this.__router.navigate(['/account/sign-in']);
  }
  dropdownMenuOpen($event: any) {
    this.settingsMenuFlag = false;
    this.integrationMenuFlag = false;
    this.notificationMenuFlag = false;
    this.notificationSettingMenuFlag = false;
    this.adminFlag = false;

    this.projectMenuFlag = !this.projectMenuFlag;
    $event.stopPropagation();
}
dropdownAdmin($event: any) {
  this.projectMenuFlag = false;
  this.settingsMenuFlag = false;
  this.notificationMenuFlag = false;
  this.notificationSettingMenuFlag = false;
  this.integrationMenuFlag = false;

  this.adminFlag = !this.adminFlag;
  $event.stopPropagation();
}

moveToNextPage(project: any) {


}
}
