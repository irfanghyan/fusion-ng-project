
import { LayoutComponent } from './layout/layout.component';
import { ProjectListingComponent } from './project-listing/project-listing.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
    {
        path: '', component: LayoutComponent,
        children: [
            {
              path: 'project-listing',
              component: ProjectListingComponent
            },
            {
              path: '',
              redirectTo: 'project-listing',
              pathMatch: 'full'
            },
            {
              path: 'application',
              loadChildren: () => import('./application/application.module')
                .then(m => m.ApplicationModule)
            },
            {
              path: 'bugs-listing',
              loadChildren: () => import('./bugs/bugs.module')
                .then(m => m.BugsModule),
            },

        ]
    }
  ]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersRoutingModule { }
