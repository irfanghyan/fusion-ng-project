import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss']
})
export class ApplicationComponent implements OnInit {

  constructor(
    private _router: Router,
  ) {

   }

  ngOnInit(): void {
  }
  tab: number = 1;
    sidebarLiElements = [
        {
            id: 1, title: 'Application', active: '',
            icon: '../../../../assets/images/releases_16.svg#releases_16'
        },
        {
            id: 2, title: 'Application Vitals', active: '',
            icon: '../../../../assets/images/releases_16.svg#releases_16'
        },
        {
            id: 3, title: 'Force Update', active: '',
            icon: '../../../../assets/images/rocket.svg#rocket'
        },

        {
            id: 8, title: 'Integration', active: '',
            icon: './../../../assets/images/network_wired.svg#network_wired'
        },

    ];

}
