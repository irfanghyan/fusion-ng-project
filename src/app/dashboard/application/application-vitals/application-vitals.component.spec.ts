import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationVitalsComponent } from './application-vitals.component';

describe('ApplicationVitalsComponent', () => {
  let component: ApplicationVitalsComponent;
  let fixture: ComponentFixture<ApplicationVitalsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApplicationVitalsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ApplicationVitalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
