import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationListingComponent } from './application-listing/application-listing.component';
import { ApplicationComponent } from './application.component';
import { ApplicationRoutingModule } from './application-routing.module';
import { ApplicationVitalsComponent } from './application-vitals/application-vitals.component';
import { ForceUpdateComponent } from './force-update/force-update.component';
import { IntegrationListingComponent } from './integration-listing/integration-listing.component';

@NgModule({
  declarations: [
    ApplicationListingComponent,
    ApplicationComponent,
    ApplicationVitalsComponent,
    ForceUpdateComponent,
    IntegrationListingComponent
  ],

  imports: [
    CommonModule,
    ApplicationRoutingModule
  ]
})
export class ApplicationModule { }
