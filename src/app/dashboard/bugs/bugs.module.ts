import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BugsListingComponent } from './bugs-listing/bugs-listing.component';
import { BugDetailsComponent } from './bug-details/bug-details.component';
import { BugViewComponent } from './bug-view/bug-view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BugsRoutingModule } from './bugs-routing.module';




@NgModule({
  declarations: [
    BugsListingComponent,
    BugDetailsComponent,
    BugViewComponent,

  ],
  imports: [
    CommonModule,
    BugsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class BugsModule { }
