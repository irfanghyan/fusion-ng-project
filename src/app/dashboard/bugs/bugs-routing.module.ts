import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BugsListingComponent } from './bugs-listing/bugs-listing.component';

const routes: Routes = [
  {

    path: '', component: BugsListingComponent,
  },
  {
    path: "bug-details/:id",
    component: BugsListingComponent
  },

];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class BugsRoutingModule { }
