import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BugsListingComponent } from './bugs-listing.component';

describe('BugsListingComponent', () => {
  let component: BugsListingComponent;
  let fixture: ComponentFixture<BugsListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BugsListingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BugsListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
