import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, FormControl, Validators, } from '@angular/forms';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  // signinForm: FormGroup
  submitted = false
  signinFlag = 2
  returnUrl: string

  signinForm: FormGroup = new FormGroup({
    seed: new FormControl(''),
    soil: new FormControl('')
  });

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,

  ) {


    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }


  ngOnInit() {
    localStorage.clear()

    this.signinForm = this.fb.group({
      seed: ['', [Validators.required, Validators.email]],
      soil: ['', [Validators.required, Validators.minLength(6)]]
    });

  }
  toggleSignin(num: number){
    this.signinFlag = num
  }

  get signinFormControls(): { [key: string]: AbstractControl } {
    return this.signinForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.signinForm.invalid) {
        return;
    }

  return  this.router.navigate(['/dashboard']);

};

  }


