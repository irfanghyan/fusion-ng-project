import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';


import { AccountRoutingModule } from './account-routing.module';

@NgModule({
    imports: [
        ReactiveFormsModule,
        AccountRoutingModule
    ],
    declarations: [


    ]
})
export class AccountModule { }
